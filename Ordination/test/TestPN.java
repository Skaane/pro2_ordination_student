package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.PN;

public class TestPN {

	@Test
	public void testopretPNGyldig() {
		PN pn = new PN(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), 3);
		assertEquals(7, pn.antalDage());
		assertEquals(3, pn.getAntalEnheder(), 0);
	}

	@Test
	public void testDosisGivesIndenForPerioden() {
		PN pn = new PN(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), 3);
		assertEquals(true, pn.givDosis(LocalDate.of(2021, 1, 3)));
	}

	@Test
	public void testDosisGivesUdenForPerioden() {
		PN pn = new PN(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), 3);
		assertEquals(false, pn.givDosis(LocalDate.of(2021, 1, 8)));
	}

	@Test
	public void testDoegnDosisDosisAntalStørreEnd0() {
		PN pn = new PN(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), 3);
		pn.givDosis(LocalDate.of(2021, 1, 1));
		pn.givDosis(LocalDate.of(2021, 1, 5));
		assertEquals(0.857, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosisDosisAntal0() {
		PN pn = new PN(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), 3);
		pn.givDosis(LocalDate.of(2021, 1, 8));
		assertEquals(0, pn.doegnDosis(), 0);
	}

	@Test
	public void testSamletDosisDageIndenFOrPeriode() {
		PN pn = new PN(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), 3);
		pn.givDosis(LocalDate.of(2021, 1, 1));
		pn.givDosis(LocalDate.of(2021, 1, 5));
		assertEquals(6, pn.samletDosis(), 0);
	}

	@Test
	public void testSamletDosisDageUdenForPeriode() {
		PN pn = new PN(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), 3);
		pn.givDosis(LocalDate.of(2021, 1, 8));
		assertEquals(0, pn.samletDosis(), 0);
	}
}
