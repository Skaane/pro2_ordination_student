package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;

public class DagligFastTest {
	@Before
	public void setUp() throws Exception {

	}

	/*
	 * Test af konstruktør.
	 */

	@Test
	public void testDagligFastMedKorrektDatoerOgdosiser() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 7);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 2, 3, 4);
		assertEquals(Arrays.toString(dagligFast.getDosiser()),
				"[Kl: 06:00   antal:  1.0, Kl: 12:00   antal:  2.0, Kl: 18:00   antal:  3.0, Kl: 23:59   antal:  4.0]");
		assertEquals(dagligFast.getStartDen(), startDen);
		assertEquals(dagligFast.getSlutDen(), slutDen);
	}

	@Test
	public void testDagligFastMedKorrektDatoerOgDosis0() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 7);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 2, 0, 4);
		assertEquals(Arrays.toString(dagligFast.getDosiser()),
				"[Kl: 06:00   antal:  1.0, Kl: 12:00   antal:  2.0, Kl: 18:00   antal:  0.0, Kl: 23:59   antal:  4.0]");
	}

	public void testDagligFastMedEnsDatoOgdosiser() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 1);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 2, 0, 4);
		assertEquals(Arrays.toString(dagligFast.getDosiser()),
				"[Kl: 06:00   antal:  1.0, Kl: 12:00   antal:  2.0, Kl: 18:00   antal:  0.0, Kl: 23:59   antal:  4.0]");
	}

	/*
	 * Test af doegnDosis().
	 */
	@Test
	public void testDoegnDosisAntalStørreEnd0() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 7);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 1, 1, 3);
		assertEquals(dagligFast.doegnDosis(), 6.0, 0);
	}

	@Test
	public void testDoegnDosisAntal0() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 7);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 1, 1, 0);
		assertEquals(dagligFast.doegnDosis(), 3.0, 0);
	}

	/*
	 * Test af samletDosis()
	 */

	@Test
	public void testSamletDosisAntalStørreEnd() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 7);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 1, 1, 3);
		assertEquals(dagligFast.samletDosis(), 42, 0);
	}

	@Test
	public void testSamletDosisAntal0() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 7);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 1, 1, 0);
		assertEquals(dagligFast.samletDosis(), 21, 0);
	}

	@Test
	public void testSamletDosisDageEns() {
		LocalDate startDen = LocalDate.of(2021, 1, 1);
		LocalDate slutDen = LocalDate.of(2021, 1, 1);
		DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 1, 1, 0);
		assertEquals(dagligFast.samletDosis(), 3, 0);
	}

}
