package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligSkaev;

public class TestDagligSkaev {

	@Test
	public void testOpretGyldigDosis() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7));
		ds.opretDosis(LocalTime.of(6, 0), 2);
		assertEquals("[Kl: 06:00   antal:  2.0]", ds.getDosiser().toString());
	}

	@Test
	public void testDoegnDosisDosisIkkeNull() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7));
		ds.opretDosis(LocalTime.of(6, 0), 2);
		ds.opretDosis(LocalTime.of(12, 0), 3);
		assertEquals(5, ds.doegnDosis(), 0);
	}

	@Test
	public void testSamletDosisDosisKorrektAntal() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7));
		ds.opretDosis(LocalTime.of(6, 0), 2);
		ds.opretDosis(LocalTime.of(12, 0), 3);
		assertEquals(35, ds.samletDosis(), 0);
	}

	@Test
	public void testSamletDosisDosisforkert() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7));
		ds.opretDosis(LocalTime.of(6, 0), 0);
		assertEquals(0, ds.samletDosis(), 0);
	}
}
