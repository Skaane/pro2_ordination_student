package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class TestController {
	Controller controller = Controller.getController();
	Patient patient;
	Laegemiddel laegemiddel;

	@Before
	public void setup() {
		controller.createSomeObjects();
		patient = controller.getAllPatienter().get(2);
		laegemiddel = controller.getAllLaegemidler().get(1);
	}

	/*
	 * TEST AF opretPNOrdination
	 */

	@Test
	public void testOpretPNGyldig() {
		PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), patient, laegemiddel,
				3);
		assertEquals(laegemiddel, pn.getLaegemiddel());
		assertEquals(7, pn.antalDage());
		assertEquals(3, pn.getAntalEnheder(), 0);
	}

	@Test
	public void testOpretPnUgyldigStartDatoEfterSlut() {
		try {
			PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 7), LocalDate.of(2021, 1, 1), patient,
					laegemiddel, 3);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "StartDato er efter SlutDato");
		}
	}

	@Test
	public void testOpretPNUgyldigPatientNull() {
		patient = null;
		try {
			PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 7), LocalDate.of(2021, 1, 1), patient,
					laegemiddel, 3);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Patient kan ikke være Null");
		}
	}

	@Test
	public void testOpretPNUgyldigLMNull() {
		laegemiddel = null;
		try {
			PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), patient,
					laegemiddel, 3);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Lægemiddel kan ikke være Null");
		}
	}

	@Test
	public void testOpretPNUgyldigAntal() {
		try {
			PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 7), LocalDate.of(2021, 1, 1), patient,
					laegemiddel, -3);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Antal skal være >=0");
		}
	}

	/*
	 * TEST AF opretDagligFastOrdination
	 */

	@Test
	public void testOpretDagligFastGyldig() {
		DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
				patient, laegemiddel, 1, 1, 1, 1);
		assertEquals(laegemiddel, df.getLaegemiddel());
		assertEquals(7, df.antalDage());
		assertEquals(
				"[Kl: 06:00   antal:  1.0, Kl: 12:00   antal:  1.0, Kl: 18:00   antal:  1.0, Kl: 23:59   antal:  1.0]",
				Arrays.toString(df.getDosiser()));
	}

	@Test
	public void testOpretDagligFastUgyldigStartDatoEfterSlut() {
		try {
			DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2021, 1, 8), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, 1, 1, 1, 1);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "StartDato er efter SlutDato");
		}
	}

	@Test
	public void testOpretDagligFastUgyldigPatientNull() {
		patient = null;
		try {
			DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, 1, 1, 1, 1);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Patient kan ikke være Null");
		}
	}

	@Test
	public void testOpretDagligFastUgyldigLMNull() {
		laegemiddel = null;
		try {
			DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, 1, 1, 1, 1);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Lægemiddel kan ikke være Null");
		}
	}

	@Test
	public void testOpretDagligFastUgyldigAntal() {
		try {
			DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, 1, 1, -3, 1);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Antal skal være >=0");
		}
	}

	/*
	 * TEST AF opretDagligSkaevOrdination
	 */

	@Test
	public void testOpretDagligSkaevGyldig() {
		LocalTime[] klokkeSlet = { LocalTime.of(6, 0) };
		double[] antalEnheder = { 2 };
		DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
				patient, laegemiddel, klokkeSlet, antalEnheder);
		assertEquals(laegemiddel, ds.getLaegemiddel());
		assertEquals(7, ds.antalDage());
		assertEquals("[Kl: 06:00   antal:  2.0]", ds.getDosiser().toString());
	}

	@Test
	public void testOpretDagligSkaevUgyldigStartDatoEfterSlut() {
		LocalTime[] klokkeSlet = { LocalTime.of(6, 0) };
		double[] antalEnheder = { 2 };
		try {
			DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 8), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "StartDato er efter SlutDato");
		}
	}

	@Test
	public void testOpretDagligSkaevUgyldigPatientNull() {
		patient = null;
		LocalTime[] klokkeSlet = { LocalTime.of(6, 0) };
		double[] antalEnheder = { 2 };
		try {
			DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Patient kan ikke være Null");
		}
	}

	@Test
	public void testOpretDagligSkaevUgyldigLMNull() {
		laegemiddel = null;
		LocalTime[] klokkeSlet = { LocalTime.of(6, 0) };
		double[] antalEnheder = { 2 };
		try {
			DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Lægemiddel kan ikke være Null");
		}
	}

	@Test
	public void testOpretDagligSkaevUgyldigAntal() {
		LocalTime[] klokkeSlet = { LocalTime.of(6, 0), LocalTime.of(7, 0), LocalTime.of(8, 0), LocalTime.of(9, 0),
				LocalTime.of(10, 0) };
		double[] antalEnheder = { -1, 2, 3, 4, 5 };
		try {
			DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Antal skal være >0");
		}

	}

	@Test
	public void testOpretDagligSkaevUgyldigLængdePåArrays() {
		LocalTime[] klokkeSlet = { LocalTime.of(6, 0), LocalTime.of(7, 0), LocalTime.of(8, 0), LocalTime.of(9, 0),
				LocalTime.of(10, 0) };
		double[] antalEnheder = { 1, 2, 3, 4, 5, 6 };
		try {
			DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"Længden på arrayet med klokkeSlet er ikke samme længde som arrayet med antalEnheder");
		}

	}

	/*
	 * TEST AF ordinationPNAnvendt
	 */

	@Test
	public void testOrdinationPNAnvendtGyldigDato() {
		PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), patient, laegemiddel,
				3);
		assertEquals(0, pn.getAntalGangeGivet());
		controller.ordinationPNAnvendt(pn, LocalDate.of(2021, 1, 2));
		assertEquals(1, pn.getAntalGangeGivet());
	}

	@Test
	public void testOrdinationPNAnvendtUgyldigDato() {
		PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), patient, laegemiddel,
				3);
		try {
			controller.ordinationPNAnvendt(pn, LocalDate.of(2021, 1, 8));
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Dato er ikke inden for gyldighedsperioden");
		}
	}

	@Test
	public void testOrdinationPNAnvendtUgyldigOrdination() {
		try {
			controller.ordinationPNAnvendt(null, LocalDate.of(2021, 1, 8));
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Ordination kan ikke være null");
		}
	}

	@Test
	public void testOrdinationPNAnvendtDatoNull() {
		PN pn = controller.opretPNOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 7), patient, laegemiddel,
				3);
		try {
			controller.ordinationPNAnvendt(pn, null);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Dato kan ikke være null");
		}
	}

	/*
	 * TEST AF anbefaletDosisPrDoegn
	 */

	@Test
	public void testAnbefaletDosisPrDoegnPatientVægtUnder25() {
		patient.setVaegt(18);
		assertEquals(1, controller.anbefaletDosisPrDoegn(patient, laegemiddel), 0);
	}

	@Test
	public void testAnbefaletDosisPrDoegnPatientVægt25() {
		patient.setVaegt(25);
		assertEquals(1.5, controller.anbefaletDosisPrDoegn(patient, laegemiddel), 0);
	}

	@Test
	public void testAnbefaletDosisPrDoegnPatientVægtOver25() {
		patient.setVaegt(50);
		assertEquals(1.5, controller.anbefaletDosisPrDoegn(patient, laegemiddel), 0);
	}

	@Test
	public void testAnbefaletDosisPrDoegnPatientVægt120() {
		patient.setVaegt(120);
		assertEquals(1.5, controller.anbefaletDosisPrDoegn(patient, laegemiddel), 0);
	}

	@Test
	public void testAnbefaletDosisPrDoegnPatientVægtOver120() {
		patient.setVaegt(168);
		assertEquals(2, controller.anbefaletDosisPrDoegn(patient, laegemiddel), 0);
	}

	@Test
	public void testAnbefaletDosisPrDoegnPatientNull() {
		patient = null;
		try {
			controller.anbefaletDosisPrDoegn(patient, laegemiddel);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Patient kan ikke være null");
		}
	}

	@Test
	public void testAnbefaletDosisPrDoegnPatientLMNull() {
		laegemiddel = null;
		try {
			controller.anbefaletDosisPrDoegn(patient, laegemiddel);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Laegemiddel kan ikke være null");
		}
	}
}
