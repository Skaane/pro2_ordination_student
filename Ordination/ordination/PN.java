package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> ordinationGivet = new ArrayList<>();

	public PN(LocalDate start, LocalDate slut, double antalEnheder) {
		super(start, slut);

		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (!(givesDen.isBefore(getSlutDen()) && givesDen.isAfter(getStartDen()) || givesDen.equals(getStartDen())
				|| givesDen.equals(getSlutDen()))) {
			return false;
		}
		ordinationGivet.add(givesDen);
		return true;
	}

	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * antalEnheder;
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();

	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return ordinationGivet.size();

	}

	public ArrayList<LocalDate> getOrdinationGivet() {
		return new ArrayList<>(ordinationGivet);
	}

	@Override
	public String getType() {
		return "pro neccesare";
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public void setAntalEnheder(double antalEnheder) {
		this.antalEnheder = antalEnheder;
	}

}
