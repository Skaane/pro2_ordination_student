package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private Dosis[] dosiser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal) {
		super(startDen, slutDen);
		dosiser[0] = opretDosis(LocalTime.of(6, 0), morgenAntal);
		dosiser[1] = opretDosis(LocalTime.of(12, 0), middagAntal);
		dosiser[2] = opretDosis(LocalTime.of(18, 0), aftenAntal);
		dosiser[3] = opretDosis(LocalTime.of(23, 59), natAntal);
	}

	private Dosis opretDosis(LocalTime tid, double antal) {
		return new Dosis(tid, antal);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis dosis : dosiser) {
			sum += dosis.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig fast";
	}

	public Dosis[] getDosiser() {
		return dosiser.clone();
	}

	public Dosis getDosis(int i) {
		return dosiser[i];
	}

}